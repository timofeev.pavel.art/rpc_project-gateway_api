package config

import (
	"os"
	"strconv"
	"time"

	"go.uber.org/zap"
)

const (
	AppName = "APP_NAME"

	serverPort         = "SERVER_PORT"
	envShutdownTimeout = "SHUTDOWN_TIMEOUT"
	envAccessTTL       = "ACCESS_TTL"
	envRefreshTTL      = "REFRESH_TTL"
	envVerifyLinkTTL   = "VERIFY_LINK_TTL"

	parseShutdownTimeoutError    = "config: parse server shutdown timeout error"
	parseRpcShutdownTimeoutError = "config: parse rpc server shutdown timeout error"
	parseTokenTTlError           = "config: parse token ttl error"
)

//go:generate easytags $GOFILE yaml

type AppConf struct {
	AppName     string    `yaml:"app_name"`
	Environment string    `yaml:"environment"`
	Domain      string    `yaml:"domain"`
	APIUrl      string    `yaml:"api_url"`
	Server      Server    `yaml:"server"`
	Token       Token     `yaml:"token"`
	Provider    Provider  `yaml:"provider"`
	Logger      Logger    `yaml:"logger"`
	UserRPC     ClientRPC `yaml:"user_rpc"`
	AuthRPC     ClientRPC `yaml:"auth_rpc"`
	ExchRPC     ClientRPC `yaml:"exch_rpc"`
}

type ClientRPC struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
	Type string `yaml:"type"`
}

type Logger struct {
	Level string `yaml:"level"`
}

type Email struct {
	VerifyLinkTTL time.Duration `yaml:"verify_link_ttl"`
	From          string        `yaml:"from"`
	Port          string        `yaml:"port"`
	Credentials   Credentials   `json:"-" yaml:"credentials"`
}

type Provider struct {
	Email Email `yaml:"email"`
	Phone Phone `yaml:"phone"`
}

type Phone struct {
	VerifyCodeTTL time.Duration `yaml:"verify_code_ttl"`
	Credentials   Credentials   `json:"-" yaml:"credentials"`
}

type Credentials struct {
	Host        string `json:"-" yaml:"host"`
	Login       string `json:"-" yaml:"login"`
	Password    string `json:"-" yaml:"password"`
	AccessToken string `json:"-" yaml:"access_token"`
	Secret      string `json:"-" yaml:"secret"`
	Key         string `json:"-" yaml:"key"`
	FilePath    string `json:"-" yaml:"file_path"`
}

type Token struct {
	AccessTTL     time.Duration `yaml:"access_ttl"`
	RefreshTTL    time.Duration `yaml:"refresh_ttl"`
	AccessSecret  string        `yaml:"access_secret"`
	RefreshSecret string        `yaml:"refresh_secret"`
}

func NewAppConf() AppConf {
	port := os.Getenv(serverPort)

	return AppConf{
		AppName: os.Getenv(AppName),
		Server: Server{
			Port: port,
		},
	}
}

func (a *AppConf) Init(logger *zap.Logger) {
	shutDownTimeOut, err := strconv.Atoi(os.Getenv(envShutdownTimeout))
	if err != nil {
		logger.Fatal(parseShutdownTimeoutError)
	}
	shutDownTimeout := time.Duration(shutDownTimeOut) * time.Second
	if err != nil {
		logger.Fatal(parseRpcShutdownTimeoutError)
	}

	var accessTTL int
	accessTTL, err = strconv.Atoi(os.Getenv(envAccessTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}
	a.Token.AccessTTL = time.Duration(accessTTL) * time.Minute
	var refreshTTL int
	refreshTTL, err = strconv.Atoi(os.Getenv(envRefreshTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}
	var verifyLinkTTL int
	verifyLinkTTL, err = strconv.Atoi(os.Getenv(envVerifyLinkTTL))
	if err != nil {
		logger.Fatal(parseTokenTTlError)
	}

	a.Provider.Email.From = os.Getenv("EMAIL_FROM")
	a.Provider.Email.Port = os.Getenv("EMAIL_PORT")
	a.Provider.Email.Credentials.Host = os.Getenv("EMAIL_HOST")
	a.Provider.Email.Credentials.Login = os.Getenv("EMAIL_LOGIN")
	a.Provider.Email.Credentials.Password = os.Getenv("EMAIL_PASSWORD")
	a.Token.AccessSecret = os.Getenv("ACCESS_SECRET")
	a.Token.RefreshSecret = os.Getenv("REFRESH_SECRET")
	a.Domain = os.Getenv("DOMAIN")
	a.APIUrl = os.Getenv("API_URL")

	a.UserRPC.Host = os.Getenv("USER_RPC_HOST")
	a.UserRPC.Port = os.Getenv("USER_RPC_PORT")
	a.UserRPC.Type = os.Getenv("USER_RPC_TYPE")
	a.AuthRPC.Host = os.Getenv("AUTH_RPC_HOST")
	a.AuthRPC.Port = os.Getenv("AUTH_RPC_PORT")
	a.AuthRPC.Type = os.Getenv("AUTH_RPC_TYPE")
	a.ExchRPC.Host = os.Getenv("EXCHANGE_RPC_HOST")
	a.ExchRPC.Port = os.Getenv("EXCHANGE_RPC_PORT")
	a.ExchRPC.Type = os.Getenv("EXCHANGE_RPC_TYPE")

	a.Provider.Email.VerifyLinkTTL = time.Duration(verifyLinkTTL) * time.Minute

	a.Token.RefreshTTL = time.Duration(refreshTTL) * time.Hour * 24

	a.Server.ShutdownTimeout = shutDownTimeout
}

type Server struct {
	Port            string        `yaml:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}
