package docs

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/message/user_mess"
)

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body user_mess.ProfileResponse
}
