package service

import (
	"context"
	"encoding/json"
	"fmt"

	amqp "github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

type RabbitReceiver struct {
	logger *zap.Logger
}

func NewRabbitReceiver(logger *zap.Logger) *RabbitReceiver {
	return &RabbitReceiver{
		logger: logger,
	}
}

func (r *RabbitReceiver) Serve(ctx context.Context) error {
	conn, err := amqp.Dial("amqp://guest:guest@notificator:5672/")
	if err != nil {
		r.logger.Error("Notificator connect fail", zap.Error(err))
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		r.logger.Error("Notificator chan err", zap.Error(err))
		return err
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"rpc_queue",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		r.logger.Error("Notificator queue declare err", zap.Error(err))
		return err
	}

	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		r.logger.Error("Notificator cunsume err", zap.Error(err))
		return err
	}

	go func() {
		r.logger.Info("Notification receiver started")
		for d := range msgs {
			var out Push
			err = json.Unmarshal(d.Body, &out)
			r.logger.Info(fmt.Sprintf("%s %s: %v ", out.Message, out.Pair, out.Price))
		}
	}()

	select {
	case <-ctx.Done():
	}

	return nil
}

type Push struct {
	Pair    string  `json:"pair"`
	Message string  `json:"message"`
	Price   float64 `json:"price"`
}
