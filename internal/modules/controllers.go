package modules

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/internal/infrastructure/component"
	aucontroller "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/internal/modules/auth/controller"
	econtroller "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/internal/modules/exchange/controller"
	ucontroller "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/internal/modules/user/controller"
)

type Controllers struct {
	User     ucontroller.Userer
	Auth     aucontroller.Auther
	Exchange econtroller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)
	authController := aucontroller.NewAuth(services.Auth, components)
	exchangeController := econtroller.NewExchange(services.Exchange, components)
	return &Controllers{
		User:     userController,
		Auth:     authController,
		Exchange: exchangeController,
	}
}
