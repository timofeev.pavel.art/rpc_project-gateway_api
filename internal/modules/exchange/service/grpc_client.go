package service

import (
	"context"

	"go.uber.org/zap"

	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/internal/infrastructure/errors"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/processing/exchange"
	epb "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/proto/exchange_grpc"
)

type ExchangeServiceGRPC struct {
	client epb.ExchangerClient
	logger *zap.Logger
}

func NewExchangeServiceGRPC(client epb.ExchangerClient, logger *zap.Logger) *ExchangeServiceGRPC {
	return &ExchangeServiceGRPC{client: client, logger: logger}
}

func (e *ExchangeServiceGRPC) GetPriceListMax(ctx context.Context) exchange.CryptoOut {
	resp, err := e.client.GetPriceListMax(ctx, &epb.CryptoRequest{})

	if err != nil {
		e.logger.Error("GetMaxPrice error", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListMaxErr}
	}

	cur := make([]models.CryptoPrice, len(resp.Currency))
	for i, item := range resp.Currency {
		cur[i] = models.CryptoPrice{
			ID:    int(item.GetID()),
			Pair:  item.GetPair(),
			Price: item.GetPrice(),
		}
	}

	return exchange.CryptoOut{
		Currency:  cur,
		ErrorCode: int(resp.ErrorCode),
	}
}

func (e *ExchangeServiceGRPC) GetPriceListMin(ctx context.Context) exchange.CryptoOut {
	resp, err := e.client.GetPriceListMin(ctx, &epb.CryptoRequest{})

	if err != nil {
		e.logger.Error("GetMinPrice error", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListMinErr}
	}

	cur := make([]models.CryptoPrice, len(resp.Currency))
	for i, item := range resp.Currency {
		cur[i] = models.CryptoPrice{
			ID:    int(item.GetID()),
			Pair:  item.GetPair(),
			Price: item.GetPrice(),
		}
	}

	return exchange.CryptoOut{
		Currency:  cur,
		ErrorCode: int(resp.ErrorCode),
	}
}

func (e *ExchangeServiceGRPC) GetPriceListAvg(ctx context.Context) exchange.CryptoOut {
	resp, err := e.client.GetPriceListAvg(ctx, &epb.CryptoRequest{})

	if err != nil {
		e.logger.Error("GetAvgPrice error", zap.Error(err))
		return exchange.CryptoOut{ErrorCode: errors.GetPriceListAvgErr}
	}

	cur := make([]models.CryptoPrice, len(resp.Currency))
	for i, item := range resp.Currency {
		cur[i] = models.CryptoPrice{
			ID:    int(item.GetID()),
			Pair:  item.GetPair(),
			Price: item.GetPrice(),
		}
	}

	return exchange.CryptoOut{
		Currency:  cur,
		ErrorCode: int(resp.ErrorCode),
	}
}
