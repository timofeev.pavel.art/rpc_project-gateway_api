package user_mess

import (
	"gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"
)

//go:generate easytags $GOFILE
type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	IdempotencyKey string `json:"idempotency_key"`
}

type ProfileResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string      `json:"message,omitempty"`
	User    models.User `json:"user_mess,omitempty"`
}
