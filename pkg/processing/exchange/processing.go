package exchange

import "gitlab.com/timofeev.pavel.art/rpc_project-gateway_api/pkg/models"

type CryptoOut struct {
	Currency  []models.CryptoPrice `json:"currency"`
	ErrorCode int                  `json:"error_code"`
}

type CryptoIn struct {
}
